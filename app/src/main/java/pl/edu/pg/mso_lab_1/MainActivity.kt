package pl.edu.pg.mso_lab_1

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    val progressBar: ProgressBar by lazy { findViewById(R.id.progress_bar) }
    val statusInfo: TextView by lazy { findViewById(R.id.status_info) }
    val startButton: TextView by lazy { findViewById(R.id.start_button) }
    val abortButton: TextView by lazy { findViewById(R.id.abort_button) }
    private var calculationTask: CalculationTask? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun startCalculations(view: View) {
        if (calculationTask != null) {
            return
        }
        startButton.isEnabled = false
        abortButton.isEnabled = true
        statusInfo.text = "Calculating..."
        progressBar.progress = 0
        calculationTask = CalculationTask()
        calculationTask?.execute()
    }

    fun stopCalculations(view: View) {
        if (calculationTask == null) {
            return
        }
        startButton.isEnabled = true
        abortButton.isEnabled = false
        calculationTask?.cancel(true)
        calculationTask = null
        statusInfo.text = "Calculations aborted!"
        progressBar.progress = 0
    }

    inner class CalculationTask: AsyncTask<Void, Int, Void>() {
        override fun doInBackground(vararg p0: Void?): Void? {
            for (i in 1..100) {
                Thread.sleep(50)
                publishProgress(i)
            }
            return null
        }

        override fun onProgressUpdate(vararg values: Int?) {
            if (calculationTask != null) {
                progressBar.progress = values[0]!!
            }
        }

        override fun onPostExecute(result: Void?) {
            statusInfo.text = "Calculations finished!"
            progressBar.progress = 100
            startButton.isEnabled = true
            abortButton.isEnabled = false
            calculationTask = null
        }
    }
}